**Development of these projects has moved to the [Tezos repository](https://gitlab.com/tezos/tezos/).
This repository is now archived. Links to the new location of each project can
be be found below.**

This repository contains aPlonK, the SNARK proving system that powers Epoxy,
Tezos' validity rollup.

This repository is developed and maintained by the
[cryptography team](https://research-development.nomadic-labs.com/files/cryptography.html)
of Nomadic Labs.

# Structure of the repository

## aPlonK

aPlonK is a PlonK-inspired proving system which focuses on proof-aggregation
and distributed proof generation.
This project is now being developed [here](https://gitlab.com/tezos/tezos/-/tree/master/src/lib_aplonk).

## PlonK

PlonK is a modular implementation of the PlonK proving system which serves as a
basis for aPlonK.
This project is now being developed [here](https://gitlab.com/tezos/tezos/-/tree/master/src/lib_plonk).

## Plompiler

Plompiler is a monadic Domain Specific Language embedded in OCaml that can be
used to build circuits for PlonK or aPlonK.
It contains a library of gadgets including optimized implementation of the
Anemoi and Poseidon hash functions as well as Schnorr signatures on JubJub.
This project is now being developed [here](https://gitlab.com/tezos/tezos/-/tree/master/src/lib_plompiler).

## Epoxy-tx

Epoxy-tx is a transactional rollup for Epoxy which allows the transfer of
Tezos' tickets.
The statements are written using plompiler.
This project is now being developed [here](https://gitlab.com/tezos/tezos/-/tree/master/src/lib_epoxy_tx).

## Bls12-381-polynomial

Bls12-381-polynomial contains C libraries and bindings to optimized
implementations of various algorithms related to the bls12-381 curve, and
polynomials based on it's scalar field. It is based on the blst library.
This project is now being developed [here](https://gitlab.com/tezos/tezos/-/tree/master/src/lib_bls12_381_polynomial).

# Compiling

To setup correctly your local environment run the script
`./scripts/install_build_deps.sh` which will take care of creating a
local opam directory `_opam`.

# Benchmarking

## Overview

You can compare PlonK & aPlonK by running
`dune exec aplonk/test/bench.exe <n> <s> <e>`. This will run PlonK & aPlonK for
a circuit of $2^{n}$ constraints, and every power of two number of proofs
between $2^{s}$ & $2^e$ ; for instance, `dune exec aplonk/test/bench.exe 4 0 2`
will run PlonK & aPlonK 3 times for for a circuit of size 16 : for an
aggregation of 1 proof, an aggregation of 2 proofs and finally an aggregation
of 4 proofs. At the same time it will print some information about the
execution, and especially the setup, proving & verifying times (in seconds).

## Public inputs

The circuit benchmarked has two public inputs per proofs ; they are linked in a
chain and only the first of the first proof & the last of the last proof are
known to the verifier ; the meta-verification includes the check that for all
proof the second public input is equal to the first public input of the next
proof.

## Benchmark file

The times will also be printed in a file named `benchmarks`, with the following
structure :
```
log(nb_proofs aggregated)
time setup PlonK
time prove PlonK
time verify PlonK
time setup aPlonK
time prove aPlonK
time verify aPlonK
```
With the previous exemple, the `benchmarks` file will look like this :

```
0
0.011262
0.013892
0.002775
0.114852
0.137270
0.015218
1
0.011092
0.018012
0.002891
0.116403
0.144813
0.015165
2
0.011113
0.026262
0.003132
0.175841
0.251190
0.016894
```
Note that if a `benchmarks` file already exists, it will be overwritten.