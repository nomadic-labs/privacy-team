Scripts to generate Poseidon constants, from https://github.com/dusk-network/Hades252/blob/7a7a255f6cc48f892de5cf8935b5264eb8893852/assets/HOWTO.md


Change first the parameters (the width and the number of constants) in ark.rs and mds.rs and
yse
```
cargo run --bin ark --release
cargo run --bin mds --release
```
to generate the constants and the MDS matrix.
